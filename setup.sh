#!/bin/bash

THIS_DIR=$(cd $(dirname $0) && pwd)
BACKUP_DIR=$(cd $(dirname $0) && cd .. && pwd)/backup-dotfiles
PACKAGES=$(find -mindepth 1 -maxdepth 1 -type d -printf "%P\n" | grep -v "^.git$")
for package in $PACKAGES ; do
    find "$THIS_DIR/$package" -type f -printf "%P\n" \
        | sed "s%dot-%.%" \
        | while read f ; do
        ls  ~/$f
        if [ -f "~/$f" ] ; then
            if [ ! -h "~/$f" ] ; then
                d=$(dirname $f)
                echo mkdir -p $d
                echo mv "~/$f" "$BACKUP_DIR/$f"
            fi
        fi
    done
done
