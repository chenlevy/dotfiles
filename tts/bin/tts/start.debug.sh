#!/bin/bash
# A debug version of start.sh, this break apart the process and allow to pinpoint the step where thing gone awry.
F=$(tempfile)
P=$F.pronunciations
qdbus org.kde.klipper /klipper org.kde.klipper.klipper.getClipboardContents > $F
~/bin/tts/pronunciations.sed $F > $P 2> /tmp/error.tts
if [ -s $F.fixed ] ; then
    festival --tts ${F}.pronunciations
    rm -f $F $P
else
    echo error | festival --tts
    cat /tmp/error | festival --tts
    mv -f $F /tmp/content.tts
    mv -f $P /tmp/content.pronounciations.tts
fi
