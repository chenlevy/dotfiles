#!/bin/bash
D5=[0-9][0-9][0-9][0-9][0-9]
find /tmp/ -maxdepth 1 -mindepth 1 -user chenl -cmin +60 \
     \( -name "est_${D5}_${D5}" \
     -o -name "est_${D5}_${D5}\.au" \
     -o -name "audiofile_${D5}" \) \
     -exec rm -f {} \;
