#!/bin/bash

while true ; do
    ping -c1 1.1.1.1 -w1 &> /dev/null || date +"%Y-%m-%d-%H:%M%S"
    sleep 5
done
