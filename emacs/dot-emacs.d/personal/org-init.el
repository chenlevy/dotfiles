;; org-init --- load configuration as org-bable

;;; Commentary:
;; delegate the rest of the configuration to an org mode file

;;; Code:
(require 'org)

(dolist (org-babel-file
         (directory-files
          (expand-file-name "personal/babel/" user-emacs-directory)
          t "\.org$"))
  (org-babel-load-file org-babel-file)
  )

(provide 'org-init)
;;; org-init.el ends here
