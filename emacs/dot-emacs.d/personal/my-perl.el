;;; personal/my-perl -- s1enhace perl experience
;;; Commentary:
;;;   ...
;;; Code:

;; c-perl mode
(defalias 'perl-mode 'cperl-mode)

(setq cperl-indent-level 2)
(custom-set-variables
 '(cperl-indent-parens-as-block t))

;; (mapc
;;  (lambda (pair)
;;    (if (eq (cdr pair) 'perl-mode)
;;        (setcdr pair 'cperl-mode)))
;;  (append auto-mode-alist interpreter-mode-alist))

;; ;; company + plsence
;; (require 'company)
;; (add-to-list 'company-backends 'company-plsense)
;; (add-hook 'perl-mode-hook 'company-mode)
;; (add-hook 'cperl-mode-hook 'company-mode)

(provide 'my-perl)
;;; my-perl.el ends here
