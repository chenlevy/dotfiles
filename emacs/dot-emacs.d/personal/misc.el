;;; personal/misc -- misc defs
;;; Commentary:
;;;   Things I don't know where else to put
;;; Code:

;; Default file encoding
;; see: http://ergoemacs.org/emacs/emacs_encoding_decoding_faq.html
(set-language-environment "UTF-8")
;; also see: to https://stackoverflow.com/a/25057485/110488
;; (modify-coding-system-alist 'file "" 'utf-8-unix)

;; whitespace-mode
(require 'whitespace)
(add-hook 'whitespace-mode-hook
          (lambda ()
            (setq whitespace-style '(face tabs empty trailing))
            (setq whitespace-active-style '(face tabs empty trailing))))

;; hide-show-mode
(require 'hideshow)
;; (add-hook 'hs-minor-mode
;;           (lambda ()
;;             (define-key hs-minor-mode-map (kbd "C-<tab>") #'hs-toggle-hiding)))
(define-key hs-minor-mode-map (kbd "C-<tab>") #'hs-toggle-hiding)

;; ;; es-mode
;; ;; (add-to-list 'load-path "/path/to/es-mode-dir")
;; (autoload 'es-mode "es-mode.el"
;;   "Major mode for editing Elasticsearch queries" t)
;; (add-to-list 'auto-mode-alist '("\\.es$" . es-mode))

;; ;; org + es
;; (org-babel-do-load-languages
;;  'org-babel-load-languages
;;  '((elasticsearch . t)))

;; How to get current buffer's filename in emacs?
;; http://unix.stackexchange.com/a/45381/5235
(defun insert-file-name ()
  "Insert the full path file name into the current buffer."
  (interactive)
  (insert (buffer-file-name (window-buffer (minibuffer-selected-window)))))

;; SQL Indent
(eval-after-load "sql"
  '(load-library "sql-indent"))

(defun sql-indent-string ()
  "Indents the string under the cursor as SQL."
  (interactive)
  (save-excursion
    (er/mark-inside-quotes)
    (let* ((text (buffer-substring-no-properties (region-beginning)
                                                 (region-end)))
           (pos (region-beginning))
           (column (progn (goto-char pos) (current-column)))
           (formatted-text
            (with-temp-buffer
              (insert text)
              (delete-trailing-whitespace)
              (sql-indent-buffer)
              (replace-string "\n"
                              (concat "\n"
                                      (make-string column
                                                   (string-to-char " ")))
               nil (point-min) (point-max))
              (buffer-string))))
      (delete-region (region-beginning) (region-end))
      (goto-char pos)
      (insert formatted-text))))

(provide 'misc)
;;; misc.el ends here
