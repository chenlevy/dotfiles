(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-hscroll-mode nil)
 '(aw-fair-aspect-ratio 2)
 '(aw-keys (quote (97 98 99 100 101 102 103 104 105)))
 '(blacken-line-length 79)
 '(bm-highlight-style (quote bm-highlight-only-fringe))
 '(bm-recenter t)
 '(cperl-indent-parens-as-block t)
 '(ein:use-auto-complete t)
 '(electric-pair-mode nil)
 '(elpy-rpc-python-command "python3.6")
 '(elpy-test-pytest-runner-command (quote ("py.test")))
 '(elpy-test-runner (quote elpy-test-pytest-runner))
 '(es-always-pretty-print t)
 '(exec-path
   (quote
    ("/home/chenl/bin" "/usr/local/sbin" "/usr/local/bin" "/usr/sbin" "/usr/bin" "/sbin" "/bin" "/usr/games" "/usr/local/games" "/snap/bin" "/home/chenl/.local/bin" "/home/chenl/.local/lib/python/bin" "/usr/lib/emacs/27.1/x86_64-linux-gnu")))
 '(ido-buffer-disable-smart-matches nil)
 '(ido-enable-flex-matching t)
 '(ido-everywhere t)
 '(ido-ignore-directories
   (quote
    ("\\`CVS/" "\\`\\.\\./" "\\`\\./" "\\`__pycache__/")))
 '(ido-mode nil nil (ido))
 '(ido-ubiquitous-mode t)
 '(magit-am-arguments (quote ("--3way")))
 '(magit-fetch-arguments nil)
 '(magit-submodule-arguments nil)
 '(mouse-yank-at-point t)
 '(org-tags-column -74)
 '(package-selected-packages
   (quote
    (ido-complete-space-or-hyphen see-mode sed smart-dash toml flycheck-rust racer cargo rust-mode use-package use-pakcage typo flycheck ghub hl-todo ivy magit-popup with-editor yasnippet elpy easy-kill dockerfile-mode crux bm textx-mode company-plsense plsense plsense-direx powershell apache-mode htmlize page-break-lines ido-completing-read+ sql-indent toggle-quotes markdown-mode yaml-mode slack string-inflection fic-mode toml-mode web-mode csv-mode ox-pandoc geiser json-mode js2-mode rainbow-mode elisp-slime-nav swiper helm-projectile helm rainbow-delimiters sed-mode org-bullets helm-git-grep zop-to-char zenburn-theme which-key volatile-highlights undo-tree smartrep smartparens smart-mode-line projectile ov operate-on-number move-text magit imenu-anywhere guru-mode grizzl god-mode gitignore-mode gitconfig-mode git-timemachine gist expand-region discover-my-major diminish diff-hl browse-kill-ring beacon anzu ace-window)))
 '(prelude-guru nil)
 '(python-shell-interpreter "python")
 '(scroll-bar-mode nil)
 '(slack-buffer-emojify t)
 '(smartparens-global-mode t)
 '(split-height-threshold 90)
 '(split-width-threshold 140)
 '(temporary-bookmark-p nil)
 '(whitespace-line-column nil)
 '(whitespace-style (quote (face trailing tabs empty)))
 '(yas-snippet-dirs
   (quote
    ("/home/chenl/.emacs.d/personal/snippets" "/home/chenl/.emacs.d/snippets" "/home/chenl/.emacs.d/elpa/elpy-20180406.217/snippets/" ""))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#3F3F3F" :foreground "#DCDCCC" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 177 :width normal :foundry "JB" :family "JetBrains Mono"))))
 '(aw-leading-char-face ((t (:inherit aw-mode-line-face :height 3.0 :width extra-expanded))))
 '(whitespace-line ((t nil))))
